# README #

Hobby project for testing the capabilities of a .Net core UWP app on a Raspberry Pi 3B coupled with a 7" touch display.
The application shows weather forecast as well as bus and train departures from the stations closest to my home in Copenhagen 

The yr.no API is used for acquiring weather forecast data and the rejseplanen.dk API is used for public transport data.