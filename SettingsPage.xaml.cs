﻿using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.WiFi;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace InfoScreen
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SettingsPage : Page, INotifyPropertyChanged
    {
        private int _brightnessPercentage;
        public int BrightnessPercentage {
            get { return _brightnessPercentage; }
            set {
                _brightnessPercentage = value;
                NotifyPropertyChanged("BrightnessPercentage");
            }
        }

        public SettingsPage()
        {
            this.InitializeComponent();
            this.DataContext = this;
            BrightnessPercentage = ScreenHandler.BrightnessPercentage;
            UpdateWifi();
        }

        private async void UpdateWifi()
        {
            try
            {
                WiFiAccessStatus accessStatus = await WiFiAdapter.RequestAccessAsync();

                if(accessStatus != WiFiAccessStatus.Allowed)
                {
                    Log.Logger.Error("Cannot access wifi adapter");
                    WifiSymbol.Symbol = Symbol.ZeroBars;
                    WifiNetorkTxt.Text = "Cannot access wifi adapter";
                    return;
                }
                
                var adapters = await WiFiAdapter.FindAllAdaptersAsync();
                var wifiAdapter = adapters.FirstOrDefault();
                if (wifiAdapter == null)
                {
                    Log.Logger.Error("No Wifi adapter found");
                    WifiSymbol.Symbol = Symbol.ZeroBars;
                    WifiNetorkTxt.Text = "No Wifi adapter found";
                    return;
                }
                await wifiAdapter.ScanAsync();
                var connectedProfile = await wifiAdapter.NetworkAdapter.GetConnectedProfileAsync();
                string connectedSsid = "";
                if (connectedProfile.IsWlanConnectionProfile)
                {
                    connectedSsid = connectedProfile.WlanConnectionProfileDetails.GetConnectedSsid();
                }
                else if (connectedProfile.IsWwanConnectionProfile)
                {
                    connectedSsid = connectedProfile.WwanConnectionProfileDetails.AccessPointName;
                }
                var connectedNetwork = wifiAdapter.NetworkReport.AvailableNetworks.SingleOrDefault(x => x.Ssid == connectedSsid);
                if (connectedNetwork == null)
                {
                    WifiSymbol.Symbol = Symbol.ZeroBars;
                    WifiNetorkTxt.Text = "No connected network";
                }
                else
                {
                    WifiSymbol.Symbol = WifiSymbolFromBars(connectedNetwork.SignalBars);
                    WifiNetorkTxt.Text = connectedSsid;
                }
            }
            catch(Exception ex)
            {
                WifiSymbol.Symbol = Symbol.ZeroBars;
                WifiNetorkTxt.Text = "Wifi error";
                Log.Logger.Error(ex, "Wifi error");
            }
        }

        private Symbol WifiSymbolFromBars(byte signalBars)
        {
            switch (signalBars)
            {
                case 0:
                    return Symbol.ZeroBars;
                case 1:
                    return Symbol.OneBar;
                case 2:
                    return Symbol.TwoBars;
                case 3:
                    return Symbol.ThreeBars;
                case 4:
                    return Symbol.FourBars;
                default:
                    Log.Logger.Warning($"Incorrect number of signal bars: {signalBars}");
                    return Symbol.ZeroBars;
            }
        }

        // Property Change Logic  
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        private void BrightnessSlider_PointerCaptureLost(object sender, PointerRoutedEventArgs e)
        {
            Slider slider = sender as Slider;
            if (slider != null)
            {
                ScreenHandler.SetScreenBrightness((int)slider.Value);
            }
        }

        private void ShutdownButton_Click(object sender, RoutedEventArgs e)
        {
            ShutdownManager.BeginShutdown(ShutdownKind.Shutdown, TimeSpan.FromSeconds(0));
        }

        private void RestartButton_Click(object sender, RoutedEventArgs e)
        {
            ShutdownManager.BeginShutdown(ShutdownKind.Restart, TimeSpan.FromSeconds(0));
        }

        private void TurnOffScreenButton_Click(object sender, RoutedEventArgs e)
        {
            ScreenHandler.TurnOffScreen();
        }

        private void CloseApp_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Exit();
        }

        private void RefreshWifi(object sender, RoutedEventArgs e)
        {
            UpdateWifi();
        }
    }
}
