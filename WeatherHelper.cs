﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace InfoScreen
{
    public class WeatherHelper
    {
        private const string SYMBOL_LARGE_BASEURL = "https://www.yr.no/grafikk/sym/v2016/png/100/";
        private const string SYMBOL_SMALL_BASEURL = "https://www.yr.no/grafikk/sym/v2016/png/48/";

        public static string GetLargeSymbolUrl(string symbolId)
        {
            return SYMBOL_LARGE_BASEURL + symbolId + ".png";
        }

        public static string GetSmallSymbolUrl(string symbolId)
        {
            return SYMBOL_SMALL_BASEURL + symbolId + ".png";
        }

        public static int GetFeelsLikeTemp(string temp, string wind)
        {
            if(!double.TryParse(temp, out var t) || !double.TryParse(wind, out var wMps))
            {
                return -666; //Dirty fix error
            }

            var wKph = wMps / 1000 * 60 * 60; //Wind in km/h

            //See: http://om.yr.no/symbol/effektiv-temperatur/
            var feelsLike = 13.12 + 0.6215 * t - 11.37 * Math.Pow(wKph, 0.16) + 0.3965 * t * Math.Pow(wKph, 0.16);
            return (int)Math.Round(feelsLike);
        }

        internal static string PrecipationTextFromXml(XElement precipitation)
        {
            var precipationValueMin = precipitation.Attribute("minvalue");
            var precipationValueMax = precipitation.Attribute("maxvalue");
            if (precipationValueMin == null || precipationValueMax == null)
            {
                return String.Format("{0} mm", precipitation.Attribute("value").Value);
            }
            else
            {
                return String.Format("{0} - {1} mm", precipationValueMin.Value, precipationValueMax.Value);
            }
        }
    }
}
