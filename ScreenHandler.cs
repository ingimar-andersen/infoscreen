﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.I2c;
using System.Runtime.InteropServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using Windows.System;
using Windows.UI.Xaml.Controls;
using Serilog;

namespace InfoScreen
{
    public class ScreenHandler
    {
        private const int MAX_BRIGHTNESS = 100;
        private const int MIN_BRIGHTNESS = 10;

        private static I2cDevice I2CAccel;
        private static DispatcherTimer IdleTimer = null;

        public static int BrightnessPercentage { get; private set; }

        private static bool _screenOn = true;
        public static bool ScreenOn {
            get { return _screenOn; }
            private set { _screenOn = value; }
        }
        public static long LastTouch { get; private set; }

        public static void SetScreenBrightness(int percentage)
        {

            SetScreenBrightness(percentage, isTurningOff: false);
        }

        public static void TurnOffScreen()
        {
            SetScreenBrightness(0, isTurningOff: true);
            ScreenOn = false;
        }

        private const int MAX_RETRY_I2C = 5;
        private static async void SetScreenBrightness(int percentage, bool isTurningOff)
        {
            byte writeValue;
            if (percentage < 0 || percentage > 100)
            {
                return;
            }
            else
            {
                if (!isTurningOff)//Only save if screen is not being turned off
                {
                    BrightnessPercentage = percentage;
                }
                //Scale to min-max
                var scaledPercentage = percentage / 100.0 * (MAX_BRIGHTNESS - MIN_BRIGHTNESS) + MIN_BRIGHTNESS;

                //Scale to byte
                writeValue = Convert.ToByte((int)Math.Round(scaledPercentage / 100.0 * 127));
            }
            if(I2CAccel == null)
            {
                InitI2C();
            }

            /* Write the register settings */
            try
            {
                byte[] writeBuff = new byte[] { 0x86, writeValue }; //backlight address, brightness 0-255 
                var tryCount = 0;
                var success = false;
                var result = new I2cTransferResult();
                while (tryCount < MAX_RETRY_I2C) //The I2C connection is not 100% fail safe an must therefore be retried in case of failure
                {
                    result = I2CAccel.WritePartial(writeBuff);
                    success = result.Status == I2cTransferStatus.FullTransfer;
                    if (success) break;
                    tryCount++;
                    await Task.Delay(50);
                }
                if (!success)
                {
                    Log.Logger.Error($"Error writing to screen after {MAX_RETRY_I2C} tries. Transfer status: {result.Status}");
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "Error writing to screen. Percentage: {0}, Write value: {1}", percentage, writeValue);
            }
        }

        public static void TurnOnScreen()
        {
            SetScreenBrightness(BrightnessPercentage);
            ScreenOn = true;
        }

        public static async void InitI2C()
        {
            string i2cDeviceSelector = I2cDevice.GetDeviceSelector();
            var settings = new I2cConnectionSettings(0x45)
            {
                BusSpeed = I2cBusSpeed.FastMode,
                SharingMode = I2cSharingMode.Shared,
            };
            IReadOnlyList<DeviceInformation> devices = await DeviceInformation.FindAllAsync(i2cDeviceSelector);
            var screen = await I2cDevice.FromIdAsync(devices[0].Id, settings);
            I2CAccel = screen;
        }

        internal static void Initialize()
        {
            if (IdleTimer != null && IdleTimer.IsEnabled) return;
            SetScreenBrightness(100);
            LastTouch = DateTime.Now.Ticks;
            IdleTimer = new DispatcherTimer();
            IdleTimer.Interval = TimeSpan.FromSeconds(10);
            IdleTimer.Tick += IdleTimer_Tick;
            IdleTimer.Start();
        }

        private static void IdleTimer_Tick(object sender, object e)
        {
            var idleSeconds = (DateTime.Now.Ticks - LastTouch) / TimeSpan.TicksPerSecond;
            if (idleSeconds > 300)
            {
                TurnOffScreen();
            }
        }

        internal static void Touched(RoutedEventArgs e)
        {
            LastTouch = DateTime.Now.Ticks;
            if (!ScreenOn)
            {
                if (e != null)
                {
                    switch (e)
                    {
                        case PointerRoutedEventArgs p:
                            p.Handled = true;
                            break;
                        case TappedRoutedEventArgs t:
                            t.Handled = true;
                            break;
                        case DoubleTappedRoutedEventArgs dt:
                            dt.Handled = true;
                            break;
                        case DragEventArgs dr:
                            dr.Handled = true;
                            break;
                        default:
                            Log.Logger.Warning($"Unknown Touched RoutedEventArgs: {e.GetType().Name}");
                            break;
                    }
                }
                TurnOnScreen();
            }
        }
    }
}
