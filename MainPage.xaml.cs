﻿using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Services.Maps;
using Windows.Storage.Streams;
using Windows.System.Threading;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace InfoScreen
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page, INotifyPropertyChanged
    {
        private const string FORECAST_URL = "https://www.yr.no/place/Denmark/Capital/Damhuss%C3%B8en/varsel.xml";

        private string _timeString;
        public string TimeString
        {
            get { return _timeString; }
            private set
            {
                _timeString = value;
                NotifyPropertyChanged("TimeString");
            }
        }

        private string _dateString;
        public string DateString
        {
            get { return _dateString; }
            private set
            {
                _dateString = value;
                NotifyPropertyChanged("DateString");
            }
        }

        private string _weatherImageMain;
        public string WeatherImageMain
        {
            get { return _weatherImageMain; }
            private set
            {
                _weatherImageMain = value;
                NotifyPropertyChanged("WeatherImageMain");
            }
        }

        private string _weatherImage1;
        public string WeatherImage1
        {
            get { return _weatherImage1; }
            private set
            {
                _weatherImage1 = value;
                NotifyPropertyChanged("WeatherImage1");
            }
        }

        private string _weatherImage2;
        public string WeatherImage2
        {
            get { return _weatherImage2; }
            private set
            {
                _weatherImage2 = value;
                NotifyPropertyChanged("WeatherImage2");
            }
        }

        private string _weatherImage3;
        public string WeatherImage3
        {
            get { return _weatherImage3; }
            private set
            {
                _weatherImage3 = value;
                NotifyPropertyChanged("WeatherImage3");
            }
        }

        private string _weatherImage4;
        public string WeatherImage4
        {
            get { return _weatherImage4; }
            private set
            {
                _weatherImage4 = value;
                NotifyPropertyChanged("WeatherImage4");
            }
        }

        private Boolean _isLoading;
        public Boolean IsLoading
        {
            get { return _isLoading; }
            private set
            {
                _isLoading = value;
                NotifyPropertyChanged("IsLoading");
            }
        }

        private Boolean _showRouteDistance;
        public Boolean ShowRouteDistance
        {
            get { return _showRouteDistance; }
            private set
            {
                _showRouteDistance = value;
                NotifyPropertyChanged("ShowRouteDistance");
            }
        }
        

        private Boolean _showHeaderTime;
        public Boolean ShowHeaderTime
        {
            get { return _showHeaderTime; }
            private set
            {
                _showHeaderTime = value;
                NotifyPropertyChanged("ShowHeaderTime");
            }
        }

        private DispatcherTimer ClockTimer;
        private DispatcherTimer FetchForecastTimer;
        private DispatcherTimer FetchTransportTimer;

        // Property Change Logic  
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public MainPage()
        {
            this.InitializeComponent();
            this.DataContext = this;
            Init();
        }

        private void Init()
        {
            //Init timers
            InitClockTimer();
            InitForecastTimer();
            InitTransportTimer();

            UpdateHeader();
            RefreshData();
        }

        public enum MapIconType
        {
            Undefined = 0,
            Station = 1,
            Home = 2,
        }

        public static BasicGeoposition HomePosition = new BasicGeoposition { Latitude = 55.675232, Longitude = 12.489785 };
        private double HomeZoomLevel = 16;

        private void InitMapLocations()
        {
            //Stations
            var locations = new List<MapElement>();
            locations.Add(CreateMapIcon("Peter Bangs Vej (Ålekistevej)\n10 22", x: 12.486116, y: 55.677495, type: MapIconType.Station));
            locations.Add(CreateMapIcon("Peter Bangs Vej (Ålholmvej)\n10 22", x: 12.487311, y: 55.676623, type:MapIconType.Station));
            locations.Add(CreateMapIcon("Peter Bangs Vej (Grøndals Parkvej)\n13 21", x: 12.487329, y: 55.677801, type: MapIconType.Station));
            locations.Add(CreateMapIcon("Ålholm Plads\n6A 26 93N", x: 12.489324, y: 55.672331, type: MapIconType.Station));
            locations.Add(CreateMapIcon("Ålholm Plads (Ålholmvej)\n10 21 22", x: 12.488660, y: 55.673221, type: MapIconType.Station));
            locations.Add(CreateMapIcon("KB Hallen St.", x: 12.491590, y: 55.678061, type: MapIconType.Station));

            var stationsLayer = new MapElementsLayer
            {
                ZIndex = 10000,
                MapElements = locations
            };
            stationsMapControl.Layers.Add(stationsLayer);

            //Home
            var homeMapIcon = CreateMapIcon("", x: 12.49014, y: 55.67527, type: MapIconType.Home, imageUri: new Uri("ms-appx:///Assets/home-pin64.png"));
            var homeLayer = new MapElementsLayer
            {
                ZIndex = 10001,
                MapElements = { homeMapIcon },
            };
            stationsMapControl.Layers.Add(homeLayer);
            
            stationsMapControl.Center = new Geopoint(HomePosition);
            stationsMapControl.ZoomLevel = HomeZoomLevel;
        }

        private async void GotoMapStartLocation()
        {
            await stationsMapControl.TrySetViewAsync(new Geopoint(HomePosition), HomeZoomLevel, 0, 0, MapAnimationKind.Linear);
        }

        private MapIcon CreateMapIcon(string title, double x, double y, MapIconType type, Uri imageUri = null) {

            BasicGeoposition position = new BasicGeoposition { Latitude = y, Longitude = x };
            Geopoint geoPoint = new Geopoint(position);
            var mapIcon = new MapIcon
            {
                Location = geoPoint,
                NormalizedAnchorPoint = new Point(0.5, 1.0),
                ZIndex = 10000,
                Title = title,
                Tag = type,
                CollisionBehaviorDesired = MapElementCollisionBehavior.RemainVisible,
            };
            if(imageUri != null)
            {
                mapIcon.Image = RandomAccessStreamReference.CreateFromUri(imageUri);
            }
            return mapIcon;
        }

        private void InitTransportTimer()
        {
            if (FetchTransportTimer != null && FetchTransportTimer.IsEnabled) return;
            FetchTransportTimer = new DispatcherTimer();
            FetchTransportTimer.Interval = TimeSpan.FromSeconds(20);
            FetchTransportTimer.Tick += FetchTransportTimer_Tick;
            FetchTransportTimer.Start();
        }

        private void InitForecastTimer()
        {
            if (FetchForecastTimer != null && FetchForecastTimer.IsEnabled) return;
            FetchForecastTimer = new DispatcherTimer();
            FetchForecastTimer.Interval = TimeSpan.FromMinutes(10);
            FetchForecastTimer.Tick += FetchForecastTimer_Tick;
            FetchForecastTimer.Start();
        }

        private void InitClockTimer()
        {
            if (ClockTimer != null && ClockTimer.IsEnabled) return;
            ClockTimer = new DispatcherTimer();
            ClockTimer.Interval = TimeSpan.FromMilliseconds(1000);
            ClockTimer.Tick += ClockTimer_Tick;
            ClockTimer.Start();
        }

        private async void FetchTransportTimer_Tick(object sender, object e)
        {
            await FetchTransportData();
        }

        private async void RefreshData()
        {
            try
            {
                IsLoading = true;

                await FetchForecastXmlData();
                await FetchTransportData();

                //Just so that is clear that the data has been refreshed
                await Task.Delay(1000);
            }
            finally
            {
                IsLoading = false;
            }
        }

        private async Task FetchTransportData()
        {
            //Quick and dirty fix
            if (pivot.SelectedIndex == 1)
            {
                try
                {
                    var departures = await TransportHelper.GetDeparturesNow(TransportHelper.KB_HALLEN_ID);
                    if (departures.Any())
                    {
                        var first = departures.First();
                        DepName1a.Text = $"{first.name} → {first.direction}";
                        DepTime1a.Text = first.time;

                        var second = departures[1];
                        DepName2a.Text = $"{second.name} → {second.direction}";
                        DepTime2a.Text = second.time;

                        var third = departures[2];
                        DepName3a.Text = $"{third.name} → {third.direction}";
                        DepTime3a.Text = third.time;

                        var fourth = departures[3];
                        DepName4a.Text = $"{fourth.name} → {fourth.direction}";
                        DepTime4a.Text = fourth.time;

                        var fifth = departures[4];
                        DepName5a.Text = $"{fifth.name} → {fifth.direction}";
                        DepTime5a.Text = fifth.time;

                        var sixth = departures[5];
                        DepName6a.Text = $"{sixth.name} → {sixth.direction}";
                        DepTime6a.Text = sixth.time;
                    }


                    var peterBangsIds = new int[] {
                        TransportHelper.PETER_BANGS_VEJ_AALH_ID,
                        TransportHelper.PETER_BANGS_VEJ_AALEK_ID,
                    };
                    departures = await TransportHelper.GetDeparturesNow(peterBangsIds);

                    if (departures.Any())
                    {
                        var first = departures.First();
                        DepName1b.Text = $"{first.name} → {first.direction}";
                        DepTime1b.Text = first.time;

                        var second = departures[1];
                        DepName2b.Text = $"{second.name} → {second.direction}";
                        DepTime2b.Text = second.time;

                        var third = departures[2];
                        DepName3b.Text = $"{third.name} → {third.direction}";
                        DepTime3b.Text = third.time;

                        var fourth = departures[3];
                        DepName4b.Text = $"{fourth.name} → {fourth.direction}";
                        DepTime4b.Text = fourth.time;

                        var fifth = departures[4];
                        DepName5b.Text = $"{fifth.name} → {fifth.direction}";
                        DepTime5b.Text = fifth.time;

                        var sixth = departures[5];
                        DepName6b.Text = $"{sixth.name} → {sixth.direction}";
                        DepTime6b.Text = sixth.time;
                    }

                    var aalholmPladsIds = new int[] {
                        TransportHelper.AALHOLM_PLADS_ID,
                        TransportHelper.AALHOLM_PLADS_ROSKILDEV_ID,
                        TransportHelper.AALHOLM_PLADS_SLOEJFEN_ID,
                    };
                    departures = await TransportHelper.GetDeparturesNow(aalholmPladsIds);

                    if (departures.Any())
                    {
                        var first = departures.First();
                        DepName1c.Text = $"{first.name} → {first.direction}";
                        DepTime1c.Text = first.time;

                        var second = departures[1];
                        DepName2c.Text = $"{second.name} → {second.direction}";
                        DepTime2c.Text = second.time;

                        var third = departures[2];
                        DepName3c.Text = $"{third.name} → {third.direction}";
                        DepTime3c.Text = third.time;

                        var fourth = departures[3];
                        DepName4c.Text = $"{fourth.name} → {fourth.direction}";
                        DepTime4c.Text = fourth.time;

                        var fifth = departures[4];
                        DepName5c.Text = $"{fifth.name} → {fifth.direction}";
                        DepTime5c.Text = fifth.time;

                        var sixth = departures[5];
                        DepName6c.Text = $"{sixth.name} → {sixth.direction}";
                        DepTime6c.Text = sixth.time;
                    }
                }
                catch(Exception ex)
                {
                    Log.Logger.Error(ex, "Error updating departures");
                }
            }
        }

        private async void FetchForecastTimer_Tick(object sender, object e)
        {
            await FetchForecastXmlData();
        }

        private void ClockTimer_Tick(object sender, object e)
        {
            TimeString = DateTime.Now.ToString("HH:mm");
            DateString = DateTime.Now.ToString("dddd, MMMM d");
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            //Map
            if (pivot.SelectedIndex == 2)
            {
                GotoMapStartLocation();
            }
            else
            {
                RefreshData();
            }
        }

        private async Task FetchForecastXmlData()
        {
            //Quick and dirty fix
            if (pivot.SelectedIndex == 0)
            {
                using (var wc = new HttpClient())
                {
                    try
                    {
                        HttpResponseMessage response = await wc.GetAsync(FORECAST_URL);
                        string responseString = await response.Content.ReadAsStringAsync();
                        var xml = XDocument.Parse(responseString);
                        ParseForecastData(xml);
                    }
                    catch (Exception ex)
                    {
                        Log.Logger.Error(ex, "Error fetching weather data");
                    }
                }
            }
        }

        private void ParseForecastData(XDocument xml)
        {
            this.Place.Text = xml.Root.Element("location").Element("name").Value;
            var count = 0;
            foreach (var element in xml.Root.Element("forecast").Element("tabular").Elements("time"))
            {
                count++;
                if(count > 5) //Take first five elements. The first one is major and the other minor.
                {
                    break;
                }
                //if(int.Parse(element.Attribute("period").Value) != 0)
                //{
                //    continue;
                //}
                var timeFrom = DateTime.Parse(element.Attribute("from").Value);
                var timeTo = DateTime.Parse(element.Attribute("to").Value);
                //this.Interval.Text = String.Format("{0} - {1}", timeFrom.ToString("HH:mm"), timeTo.ToString("HH:mm"));
                var temp = element.Element("temperature");
                var windSpeed = element.Element("windSpeed");
                var windSpeedValue = windSpeed.Attribute("mps").Value;
                var windDirection = element.Element("windDirection");
                var symbol = element.Element("symbol");
                var symbolId = symbol.Attribute("var").Value;

                var precipitation = element.Element("precipitation");
                string precText = WeatherHelper.PrecipationTextFromXml(precipitation);
                var tempValue = temp.Attribute("value").Value;
                var tempText = String.Format("{0}°", tempValue);
                var feelsLike = WeatherHelper.GetFeelsLikeTemp(tempValue, windSpeedValue);
                switch (count)
                {
                    case 1: //Major
                        {
                            this.WeatherImageMain = WeatherHelper.GetLargeSymbolUrl(symbolId);
                            this.Temperature.Text = tempText;
                            this.WindMps.Text = String.Format("{0} - {2} m/s", windSpeed.Attribute("name").Value, windDirection.Attribute("code").Value, windSpeedValue);
                            this.FeelsLike.Text = $"Feels like {feelsLike}°";
                            this.Precipitation.Text = precText;
                            break;
                        }
                    case 2:
                        {
                            this.WeatherImage1 = WeatherHelper.GetLargeSymbolUrl(symbolId);
                            this.Temp1.Text = tempText;
                            this.Time1.Text = timeFrom.ToString("HH:mm");
                            this.Prec1.Text = precText;
                            break;
                        }
                    case 3:
                        {
                            this.WeatherImage2 = WeatherHelper.GetLargeSymbolUrl(symbolId);
                            this.Temp2.Text = tempText;
                            this.Time2.Text = timeFrom.ToString("HH:mm");
                            this.Prec2.Text = precText;
                            break;
                        }
                    case 4:
                        {
                            this.WeatherImage3 = WeatherHelper.GetLargeSymbolUrl(symbolId);
                            this.Temp3.Text = tempText;
                            this.Time3.Text = timeFrom.ToString("HH:mm");
                            this.Prec3.Text = precText;
                            break;
                        }
                    case 5:
                        {
                            this.WeatherImage4 = WeatherHelper.GetLargeSymbolUrl(symbolId);
                            this.Temp4.Text = tempText;
                            this.Time4.Text = timeFrom.ToString("HH:mm");
                            this.Prec4.Text = precText;
                            break;
                        }
                    default: { break; }
                }

                //this.Description.Text = symbol.Attribute("name").Value;
                //this.Updated.Text = String.Format("Last updated: {0}", DateTime.Now.ToString("HH:mm:ss"));
            }
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SettingsPage));
        }

        private void pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateHeader();
            RefreshData();
        }

        private void UpdateHeader()
        {
            //Weather
            if (pivot.SelectedIndex == 0)
            {
                try
                {
                    FadeOutHeaderTime.Duration = new Duration(new TimeSpan(0, 0, 1));
                    FadeOutHeaderTime.Begin();
                }
                catch { }
               
                //this.ShowHeaderTime = false;

                //Highlight
                this.HeaderTransport.Background = new SolidColorBrush(Colors.Transparent);
                this.HeaderWeather.Background = new SolidColorBrush(Color.FromArgb(0xff, 0xe2, 0xe2, 0xe2));
                this.HeaderMap.Background = new SolidColorBrush(Colors.Transparent);
            }
            //Transport
            else if(pivot.SelectedIndex == 1)
            {
                try
                {
                    FadeOutHeaderTime.Duration = new Duration(new TimeSpan(0, 0, 1));
                    FadeInHeaderTime.Begin();
                }
                catch { }
                //this.ShowHeaderTime = true;

                //Highlight
                this.HeaderTransport.Background = new SolidColorBrush(Color.FromArgb(0xff,0xe2,0xe2,0xe2));
                this.HeaderWeather.Background = new SolidColorBrush(Colors.Transparent);
                this.HeaderMap.Background = new SolidColorBrush(Colors.Transparent);
            }
            //Map
            else if(pivot.SelectedIndex == 2)
            {
                try
                {
                    FadeOutHeaderTime.Duration = new Duration(new TimeSpan(0, 0, 1));
                    FadeInHeaderTime.Begin();
                }
                catch { }

                //Highlight
                this.HeaderTransport.Background = new SolidColorBrush(Colors.Transparent);
                this.HeaderWeather.Background = new SolidColorBrush(Colors.Transparent);
                this.HeaderMap.Background = new SolidColorBrush(Color.FromArgb(0xff, 0xe2, 0xe2, 0xe2));

            }
        }

        private void StationsMapControl_LoadingStatusChanged(MapControl sender, object args)
        {
            //Map
            if (pivot.SelectedIndex == 2)
            {
                IsLoading = sender.LoadingStatus == MapLoadingStatus.Loading;
            }
        }

        private async void StationsMapControl_MapElementClick(MapControl sender, MapElementClickEventArgs args)
        {
            var locations = args.MapElements.Where(x => x is MapIcon).Select(x => (MapIcon)x).ToList();
            var station = locations.FirstOrDefault(x => (MapIconType)x.Tag == MapIconType.Station);
            var home = locations.FirstOrDefault(x => (MapIconType)x.Tag == MapIconType.Home);
            if (station != null)
            {
                IsLoading = true;
                MapRouteFinderResult routeResult = await MapRouteFinder.GetWalkingRouteAsync(station.Location, new Geopoint(HomePosition));
                if (routeResult.Status != MapRouteFinderStatus.Success)
                {
                    var wayPoints = new Geopoint[]{ station.Location, new Geopoint(HomePosition)};
                    routeResult = await MapRouteFinder.GetDrivingRouteAsync(station.Location, new Geopoint(HomePosition));
                }
                if (routeResult.Status == MapRouteFinderStatus.Success)
                {
                    // Use the route to initialize a MapRouteView.
                    MapRouteView viewOfRoute = new MapRouteView(routeResult.Route);
                    viewOfRoute.RouteColor = Colors.Yellow;
                    viewOfRoute.OutlineColor = Colors.Black;

                    // Add the new MapRouteView to the Routes collection
                    // of the MapControl.
                    stationsMapControl.Routes.Clear();
                    stationsMapControl.Routes.Add(viewOfRoute);
                    RouteDistance.Text = $"{routeResult.Route.LengthInMeters} m";
                    ShowRouteDistance = true;
                    // Fit the MapControl to the route.
                    await stationsMapControl.TrySetViewBoundsAsync(routeResult.Route.BoundingBox, new Thickness(0, 60, 0, 60), MapAnimationKind.Linear);
                }
                else
                {
                    stationsMapControl.Routes.Clear();
                    RouteDistance.Text = "";
                    ShowRouteDistance = false;
                }
                IsLoading = false;
            }
            else
            {
                stationsMapControl.Routes.Clear();
                RouteDistance.Text = "";
                ShowRouteDistance = false;
                if (home != null)
                {
                    GotoMapStartLocation();
                }
            }
        }

        private void StationsMapControl_Loaded(object sender, RoutedEventArgs e)
        {
            InitMapLocations();
        }

        private void StationsMapControl_MapTapped(MapControl sender, MapInputEventArgs args)
        {
            //Workaround: This is handled globally in App.xaml.cs but the event is not fired for the map control for some reason.
            ScreenHandler.Touched(null);
        }
    }
}
